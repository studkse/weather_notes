package ch.bbw.se.notes;
import ch.bbw.se.notes.controller.MainViewController;
import ch.bbw.se.notes.model.Model;
import ch.bbw.se.notes.model.ToDo;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
/**
 * <h1>Main Application for 'Notes'</h1>
 * @author Ennin Samuel
 * @since 26.12.19
 * @version 1.0
 */
public class MainApplication extends Application {
	@Override
	public void start(Stage primaryStage) {
		try {
			//Model
			// hello
			Model myModel = new Model();
			myModel.getNotes().add(new ToDo(1,"2018-01-12", "Little", "Cooler Air Temperature", "Air temperature lower than 4 Months ago."));
			myModel.getNotes().add(new ToDo(2,"2019-03-15", "Intermediate", "Check Water Temperature", "Water Temperature below 8 degrees celsius"));
			
			//View
			FXMLLoader myLoader = new FXMLLoader(getClass().getResource("view/MainView.fxml"));
			Pane root = (Pane)myLoader.load();
			
			//Controller
			MainViewController controller = (MainViewController) myLoader.getController();
			controller.setModel(myModel);
			
			Scene scene = new Scene(root,400,400);
			scene.getStylesheets().add(getClass().getResource("view/application.css").toExternalForm());
			primaryStage.setMinWidth(720);
			primaryStage.setMinHeight(660);
			
			//Icon
			Image icon = new Image(getClass().getResourceAsStream("view/note.png"));
			primaryStage.getIcons().add(icon);
			primaryStage.setTitle("Notes Application");
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}

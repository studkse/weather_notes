package ch.bbw.se.notes.model;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * <h1>Model for the ToDo List</h1>
 * @author Ennin Samuel
 * @since 26.12.19
 * @version 1.0
 */
public class Model {
	private ObservableList<ToDo> notes;
	
	//Constructor
	public Model() {
		// Initialize all variables
		notes = FXCollections.<ToDo>observableArrayList();
		
	}

	public ObservableList<ToDo> getNotes() {
		return notes;
	}
	
}
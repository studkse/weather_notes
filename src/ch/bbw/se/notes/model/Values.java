package ch.bbw.se.notes.model;

public class Values {
	double air_temperature;
	double water_temperature;
	double wind_gust_max_10min;
	
	
	
	public Values() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Values(double air_temperature, double water_temperature, double wind_gust_max_10min) {
		super();
		this.air_temperature = air_temperature;
		this.water_temperature = water_temperature;
		this.wind_gust_max_10min = wind_gust_max_10min;
	}
	public double getAir_temperature() {
		return air_temperature;
	}
	public void setAir_temperature(double air_temperature) {
		this.air_temperature = air_temperature;
	}
	public double getWater_temperature() {
		return water_temperature;
	}
	public void setWater_temperature(double water_temperature) {
		this.water_temperature = water_temperature;
	}
	public double getWind_gust_max_10min() {
		return wind_gust_max_10min;
	}
	public void setWind_gust_max_10min(double wind_gust_max_10min) {
		this.wind_gust_max_10min = wind_gust_max_10min;
	}
	
	
}

package ch.bbw.se.notes.model;

import java.util.Date;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
/**
 * <h1>ToDo</h1>
 * @author Ennin Samuel
 * @since 26.12.19
 * @version 1.0
 */
public class ToDo {
	private StringProperty id;
	private StringProperty date;
	private StringProperty impt;
	private StringProperty keyword;
	private StringProperty content;
	
	//Constructors
	public ToDo(int id, String date, String impt, String keyword,
			String content) {
		super();
		this.id = new SimpleStringProperty(id + "");
		this.date = new SimpleStringProperty(date);
		this.impt = new SimpleStringProperty(impt);
		this.keyword = new SimpleStringProperty(keyword);
		this.content = new SimpleStringProperty(content);
	}

	//Getters and Setters

	public StringProperty getId() {
		return id;
	}

	public void setId(StringProperty id) {
		this.id = id;
	}

	public StringProperty getDate() {
		return date;
	}

	public void setDate(StringProperty date) {
		this.date = date;
	}

	public StringProperty getImpt() {
		return impt;
	}

	public void setImpt(StringProperty impt) {
		this.impt = impt;
	}

	public StringProperty getKeyword() {
		return keyword;
	}

	public void setKeyword(StringProperty keyword) {
		this.keyword = keyword;
	}

	public StringProperty getContent() {
		return content;
	}

	public void setContent(StringProperty content) {
		this.content = content;
	}
	
}

package ch.bbw.se.notes.model;

public class Weather {
	private String station;
	private String timestamp;
	Values ValuesObject;

	// Getter Methods

	public String getStation() {
		return station;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public Values getValues() {
		return ValuesObject;
	}

	// Setter Methods

	public void setStation(String station) {
		this.station = station;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public void setValues(Values valuesObject) {
		this.ValuesObject = valuesObject;
	}
}


package ch.bbw.se.notes.controller;

import java.text.DateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import ch.bbw.se.notes.model.ToDo;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class ToDoDialogController {

	@FXML
	private DatePicker dateField;

	@FXML
	private ComboBox<String> cbImportance;

	@FXML
	private TextField tfKeyword;

	@FXML
	private TextArea taContent;

	private ToDo todo;

	// Initialize
	@FXML
	public void initialize() {
		ObservableList<String> importance = FXCollections.observableArrayList("High", "Intermediate", "Little");
		cbImportance.setItems(importance);
		cbImportance.getSelectionModel().select(2);
	}

	public void setToDo(ToDo todo) {
		this.todo = todo;
		if (todo != null) {
			DateTimeFormatter form = DateTimeFormatter.ofPattern("d.MM.yyyy");
			dateField.setValue(LocalDate.parse(todo.getDate().getValue(), form));
			cbImportance.getSelectionModel().select(todo.getImpt().getValue());
			tfKeyword.setText(todo.getKeyword().getValue());
			taContent.setText(todo.getContent().getValue());
		}
	}

	public ToDo getTodo() {
		return todo;
	}

	@FXML
	private void handleOK(ActionEvent ae) {
		if (dateField.getValue() != null && (!tfKeyword.equals("")) && (!taContent.equals(""))) {
			DateTimeFormatter form = DateTimeFormatter.ofPattern("d.MM.yyyy");
			todo = new ToDo(2, dateField.getValue().format(form), cbImportance.getValue(), tfKeyword.getText(),
					taContent.getText());
			Node source = (Node) ae.getSource();
			Stage stage = (Stage) source.getScene().getWindow();
			stage.close();
		}
	}

	@FXML
	private void handleCancel(ActionEvent ae) {
		todo = null;
		Node source = (Node) ae.getSource();
		Stage stage = (Stage) source.getScene().getWindow();
		stage.close();
	}

}

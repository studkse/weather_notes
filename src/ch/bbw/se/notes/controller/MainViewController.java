package ch.bbw.se.notes.controller;

import java.io.BufferedReader;

import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.StringTokenizer;

import org.json.JSONArray;
import org.json.JSONObject;


import ch.bbw.se.notes.model.*;
import ch.bbw.se.notes.model.ToDo;
import javafx.beans.property.SimpleStringProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * <h1>Controller Class</h1>
 * 
 * @author Ennin Samuel
 * @since 12.12.18
 * @version 2.0
 */
public class MainViewController {
	private Model model;
	private ArrayList<Weather> list;

	@FXML
	private TableView<ToDo> tvNotes;

	@FXML
	private TableColumn<ToDo, String> tcId;

	@FXML
	private TableColumn<ToDo, String> tcDate;

	@FXML
	private TableColumn<ToDo, String> tcImpt;

	@FXML
	private TableColumn<ToDo, String> tcKeyword;

	@FXML
	private ContextMenu ctmNotes;
	
	@FXML
	private Label lbTitle;
	
	@FXML
	private Label lbDate;
	
	@FXML
	private Label lbContent;
	
	private String timeStamp = new SimpleDateFormat("yyyy.MM.dd").format(Calendar.getInstance().getTime());
	
	private String content = "";
	// Constructor
	public MainViewController() {
		super();
	}

	// Initialize
	@FXML
	public void initialize() {
		tcId.setCellValueFactory(todo -> todo.getValue().getId());
		tcDate.setCellValueFactory(todo -> todo.getValue().getDate());
		tcImpt.setCellValueFactory(todo -> todo.getValue().getImpt());
		tcKeyword.setCellValueFactory(todo -> todo.getValue().getKeyword());
		this.init();
	}
	
	private void init() {
		 list = new ArrayList<Weather>();
		 try {
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				Date date = new Date();
				String formatDate = dateFormat.format(date);
				System.out.println(formatDate);
				URL url = new URL("https://tecdottir.herokuapp.com/measurements/mythenquai?startDate=" + formatDate);

				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setRequestMethod("GET");
				BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));

				StringBuffer response = new StringBuffer();
				String inputLine;
				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine + "\n");
					System.out.print("hey:" + inputLine);
					System.out.println();
				}
				// Übung: Ausgabe der nächsten Verbindungen
				JSONObject mainObj = new JSONObject(response.toString());
				JSONArray result = mainObj.getJSONArray("result");

				for (int i = 0; i < 10 /*result.length() */; i++) {
					Weather weather = new Weather();
					Values values = new Values();

					String station = (String) result.getJSONObject(i).get("station");
					weather.setStation(station);

					String timestamp = (String) result.getJSONObject(i).get("timestamp");
					weather.setTimestamp(timestamp);

					JSONObject resValues = (JSONObject) result.getJSONObject(i).get("values");

					
					values.setAir_temperature(Double.parseDouble(resValues.getJSONObject("air_temperature").get("value").toString()));
					values.setWater_temperature(Double.parseDouble(resValues.getJSONObject("water_temperature").get("value").toString()));
					values.setWind_gust_max_10min(Double.parseDouble(resValues.getJSONObject("wind_gust_max_10min").get("value").toString()));
						
//						
					weather.setValues(values);
					list.add(weather);
					System.out.println("Added Weather Value[" + i + "]");
//								System.out.println(timestamp.toString());
//								System.out.println(String.format("%s %s - (Gleis: %s)", timestamp.toString(), air_temperature.toString(), water_temperature.toString()));
				}

				in.close();

			} catch (Exception e) {
				e.printStackTrace();
			}
	}

	// Setter
	public void setModel(Model model) {
		this.model = model;
		// Bindings
		tvNotes.setItems(this.model.getNotes());
		lbTitle.setText("Wetter - Mythenquai");
		lbDate.setText(timeStamp);
		lbContent.setText(this.createContent());
	}
	private String createContent() {
		String content = "";
		String vergleich = "";
		double difference = 0.0;
		if(list.get(0).getValues().getAir_temperature() < list.get(0).getValues().getWater_temperature()) {
			vergleich = "k�lter";
			difference = list.get(0).getValues().getWater_temperature() - list.get(0).getValues().getAir_temperature();
		} else {
			vergleich = "w�rmer";
			difference = list.get(0).getValues().getAir_temperature() - list.get(0).getValues().getWater_temperature();
		}
		
		
		content ="Eine aktuelle Lufttemperatur betr�gt " + list.get(0).getValues().getAir_temperature() + "�C.\nDie Lufttemperatur ist somit " + difference+ "�C " + vergleich + " als die aktuelle Wassertemperatur.\n"
				+ "Die Wassertemperatur betr�gt " +  list.get(0).getValues().getWater_temperature() +
				".\nDie Wetterstation " + list.get(0).getStation() + " erfasste eine Windgeschwindigkeit von " + list.get(0).getValues().getWind_gust_max_10min() + ".";
		return content;
	}

	@FXML
	public void handleDeleteToDo(ActionEvent ae) {
		int selId = tvNotes.getSelectionModel().getSelectedIndex();
		if (selId >= 0) {
			tvNotes.getItems().remove(selId);
		} else {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Nothing Selected");
			alert.setHeaderText("No note is selected");
			alert.setContentText("Select a note to reslove this issue");
			alert.showAndWait();
		}
	}

	@FXML
	public void handleAbout(ActionEvent ae) {

		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("About");
		alert.setHeaderText("About Notes Application");
		alert.setContentText("Write, edit and delete notes with this application.\nAmong other things you could set a date for each note.\n\nCopyright� Samuel Ennin");
		alert.showAndWait();
	}
	@FXML
	public void handleWeather(ActionEvent ae) {

		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Weather Data set");
		alert.setHeaderText("See Your Data Set");
		String content = "";
		content += "Datum:\t" + list.get(0).getTimestamp() + "\n";
		for (int i = 0; i < 3; i++) {
			
			content += "Wert[" + i + "]:\n";
			content += "Lufttemperatur:\t" + list.get(i).getValues().getAir_temperature();
			content += "\nMessung einer Windb�e (ohne Gew�hr) in m/s:\t" + list.get(i).getValues().getWind_gust_max_10min();
			content += "\nWassertemperatur:\t" + list.get(i).getValues().getWater_temperature();
			content += "\n-------------------------\n";
		}
		alert.setContentText(content +"\n\nCopyright� Samuel Ennin");
		alert.showAndWait();
	}

	@FXML
	public void handleNewToDo(ActionEvent ae) {
		int id;
		ToDo todo = showToDoDialog(null, "New Note");
		if (todo != null) {
			id = model.getNotes().size() + 1;
			todo.setId(new SimpleStringProperty(id + ""));
			model.getNotes().add(todo);

		}
	}

	@FXML
	public void handleEditToDo(ActionEvent ae) {
		int id;
		ToDo selTodo = tvNotes.getSelectionModel().getSelectedItem();
		if (selTodo != null) {
			ToDo todo = showToDoDialog(selTodo, "Edit note");
			if (todo != null) {
				id = model.getNotes().indexOf(selTodo) + 1;
				model.getNotes().remove(selTodo);
				todo.setId(new SimpleStringProperty(id + ""));
				model.getNotes().add(todo);
			}
		} else {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Nothing Selected");
			alert.setHeaderText("No note is selected");
			alert.setContentText("Select a note to reslove this issue");
			alert.showAndWait();
		}
	}

	// showToDoDialog
	private ToDo showToDoDialog(ToDo todo, String title) {
		try {

			// View
			FXMLLoader myLoader = new FXMLLoader();
			myLoader.setLocation(MainViewController.class.getResource("view/ToDoView.fxml"));
			Pane page = (Pane) myLoader.load();

			// Controller
			ToDoDialogController controller = myLoader.getController();
			controller.setToDo(todo);
			;

			Stage dialogStage = new Stage();
			dialogStage.setTitle(title);
			dialogStage.initModality(Modality.APPLICATION_MODAL);
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);

			dialogStage.showAndWait();
			return controller.getTodo();
		} catch (IOException e) {
			System.out.println("Problem here");
			e.printStackTrace();
			return null;
		}
	}

}
